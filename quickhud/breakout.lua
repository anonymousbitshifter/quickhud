--- @type Mq
local mq = require('mq')

--- @type ImGui
require 'ImGui'

-- Logging
local log = require('quickhud.lib.Write')

-- Libraries
local ICON = require('quickhud.lib.icons')
local utils = require('quickhud.utils')
local uiComponents = require('quickhud.uihelpers')
local services = require('quickhud.services')
local state = require('quickhud.state')
local COLOR = require('quickhud.lib.color')

local linq = require("quickhud.lib.lazylualinq.linq")

-- Services
local navService = services.Nav
local zoneDataService = services.ZoneData
local playerActionsService = services.PlayerActions

-- module specific

local TEXT_BASE_WIDTH, _ = ImGui.CalcTextSize("A")
local TEXT_BASE_HEIGHT = ImGui.GetTextLineHeightWithSpacing();

local TABLE_FLAGS = bit32.bor(
    ImGuiTableFlags.Hideable,
    ImGuiTableFlags.RowBg,
    ImGuiTableFlags.ScrollX,
    ImGuiTableFlags.ScrollY,
    ImGuiTableFlags.BordersOuter,
    ImGuiTableFlags.Resizable,
    ImGuiTableFlags.Reorderable,
    ImGuiTableFlags.SizingFixedFit,
    ImGuiTableFlags.Sortable
)

local COLUMN_ID = {
    NAME = 1,
    LOS = 2,
    DISTANCE = 3,
    LEVEL = 4,
    RACE = 5,
    CLASS = 6,
    HP = 7,
    LOCATION = 8,
    NAMED = 9
}

function cachedSpawnData(spawnId, columnId, accessor)
    return utils.getCachedData(
        string.format("spawnentry-%s-%s", spawnId, columnId),
        accessor,
        0.3
    )
end

local sortMethods = {
    [COLUMN_ID.NAME] = function(element) return cachedSpawnData(element.id, COLUMN_ID.NAME, (function() return element.spawn.CleanName() or "" end)) end,
    [COLUMN_ID.LOS] = function(element) return cachedSpawnData(element.id, COLUMN_ID.LOS, (function() return element.spawn.LineOfSight() and 1 or 0 end)) end,
    [COLUMN_ID.DISTANCE] = function(element) return cachedSpawnData(element.id, COLUMN_ID.DISTANCE, (function() return tonumber(element.spawn.Distance3D() or 0) end)) end,
    [COLUMN_ID.LEVEL] = function(element) return cachedSpawnData(element.id, COLUMN_ID.LEVEL, (function() return tonumber(element.spawn.Level() or 0) end)) end,
    [COLUMN_ID.RACE] = function(element) return cachedSpawnData(element.id, COLUMN_ID.RACE, (function() return element.spawn.Race() or "" end)) end,
    [COLUMN_ID.CLASS] = function(element) return cachedSpawnData(element.id, COLUMN_ID.CLASS, (function() return element.spawn.Class() or "" end)) end,
    [COLUMN_ID.HP] = function(element) return cachedSpawnData(element.id, COLUMN_ID.HP, (function() return tonumber(element.spawn.PctHPs() or 0) end)) end,
    [COLUMN_ID.LOCATION] = function(element) return cachedSpawnData(element.id, COLUMN_ID.LOCATION, (function() return element.spawn.LocYXZ() or "" end)) end,
    [COLUMN_ID.NAMED] = function(element) return cachedSpawnData(element.id, COLUMN_ID.NAMED, (function() return element.spawn.Named() and 1 or 0 end)) end
}

local columnFlags = bit32.bor(ImGuiTableColumnFlags.WidthFixed, ImGuiTableColumnFlags.DefaultSort)
local defaultColumnWidth = -1.0

local TABLE_COLUMNS = {
    {label = ICON.MD_PERSON.." Name", flags = columnFlags, width = defaultColumnWidth, id = COLUMN_ID.NAME, order = 0},
    {label = ICON.FA_EYE, flags = columnFlags, width = defaultColumnWidth, id = COLUMN_ID.LOS, order = 1},
    {label = ICON.FA_ARROWS_H.." Distance", flags = columnFlags, width = defaultColumnWidth, id = COLUMN_ID.DISTANCE, order = 2},
    {label = ICON.FA_HASHTAG.." Level", flags = columnFlags, width = defaultColumnWidth, id = COLUMN_ID.LEVEL, order = 3},
    {label = ICON.MD_PERSON_OUTLINE.." Race", flags = columnFlags, width = defaultColumnWidth, id = COLUMN_ID.RACE, order = 4},
    {label = ICON.MD_FOLDER_SHARED.." Class", flags = columnFlags, width = defaultColumnWidth, id = COLUMN_ID.CLASS, order = 5},
    {label = ICON.FA_MEDKIT.." Health", flags = columnFlags, width = defaultColumnWidth, id = COLUMN_ID.HP, order = 6},
    {label = ICON.FA_LOCATION_ARROW.." Location", flags = columnFlags, width = defaultColumnWidth, id = COLUMN_ID.LOCATION, order = 7}
}

local CONSIDER_SIGN_LOOKUP = {
    YELLOW = 1,
    RED = 1,
    WHITE = 0,
    BLUE = -1,
    LIGHTBLUE = -1,
    GREEN = -1,
    GREY = -1
}

local activeSorts = {}

local zoneBreakdownTree = {}

function displayFullZoneBreakdown()
    ImGui.TextColored(0, 1, 0, 1, string.format("%s %s Players", mq.TLO.SpawnCount("PC")(), ICON.MD_PERSON))
    ImGui.SameLine()
    ImGui.Text(string.format("in %s", mq.TLO.Zone.Name()))
    local closeText = ICON.MD_CLOSE
    ImGui.SameLine(ImGui.GetWindowContentRegionWidth() - ((TEXT_BASE_WIDTH * string.len(closeText)) + 2))
    if ImGui.SmallButton(closeText) then
        state.breakdownOpen = false
    end
    uiComponents.ToolTip("Close the zone breakout window.")

    local myName = mq.TLO.Me.CleanName()

    -- Ideas:
    -- - tabs for
    -- - - custom spawn filter ie: (pc group healer)
    -- - filters for level / race / class / distance or custom filter
    if ImGui.BeginTabBar("quickhud##breakdownTabs") then
        if ImGui.BeginTabItem("By Guild##breakdownTab") then
            displayZoneBreakdownByGuild()
            ImGui.EndTabItem()
        end

        if ImGui.BeginTabItem("All Players##breakdownTab") then
            local players = zoneDataService:GetPlayers()
            if(nil ~= players and nil ~= next(players)) then
                ImGui.Text(string.format("There are %d %s(s) in the zone", #players, "players"))
                renderBreakdownTable("allplayers", TABLE_COLUMNS, players, -1)
            end
            ImGui.EndTabItem()
        end

        if ImGui.BeginTabItem("All NPCs##breakdownTab") then
            local npcs = zoneDataService:GetNpcs()
            if(nil ~= npcs and nil ~= next(npcs)) then
                ImGui.Text(string.format("There are %d %s(s) in the zone", #npcs, "NPCs"))
                renderBreakdownTable("allnpcs", TABLE_COLUMNS, npcs, -1)
            end
            ImGui.EndTabItem()
        end

        if ImGui.BeginTabItem("Named NPCs##breakdownTab") then
            local named = zoneDataService:GetNamed()
            if(nil ~= named and nil ~= next(named)) then
                ImGui.Text(string.format("There are %d %s(s) in the zone", #named, "named"))
                renderBreakdownTable("allnamed", TABLE_COLUMNS, named, -1)
            end
            ImGui.EndTabItem()
        end
        ImGui.EndTabBar()
    end
end

function displayZoneBreakdownByGuild()
    local zoneBreakdown = zoneDataService:GetGuilds()
    if(nil ~= zoneBreakdown and nil ~= next(zoneBreakdown)) then
        for index, record in ipairs(zoneBreakdown) do
            local guild = record.guild
            local playersInGuild = record.players
            ImGui.BeginGroup()
            ImGui.Text(zoneBreakdownTree[guild] and ICON.FA_CHEVRON_DOWN or ICON.FA_CHEVRON_RIGHT)
            ImGui.SameLine()
            ImGui.Text(string.format("%s x", record.count))
            ImGui.SameLine()
            local guildText = string.format("<%s>", guild)
            if guild == mq.TLO.Me.Guild() then
                uiComponents.TextColored(guildText, {0, 1, 0, 1})
                uiComponents.ToolTip("Your guild")
            else
                ImGui.Text(guildText)
            end
            ImGui.EndGroup()

            if ImGui.IsItemClicked() then
                if nil == zoneBreakdownTree[guild] then
                    zoneBreakdownTree[guild] = true
                else
                    zoneBreakdownTree[guild] = not zoneBreakdownTree[guild]
                end
            end

            if zoneBreakdownTree[guild] then
                ImGui.Indent()

                renderBreakdownTable(guild, TABLE_COLUMNS, playersInGuild)

                ImGui.Unindent()
            end
        end
    end
end

function sortDelegate(sortMethod, direction)
    return function(queryable)
        if direction == ImGuiSortDirection.Ascending then
            queryable = queryable:orderBy(sortMethod)
        else
            queryable = queryable:orderByDescending(sortMethod)
        end

        return queryable
    end
end

function renderBreakdownTable(tableKey, columns, records, tableHeight)
    local myId = mq.TLO.Me.ID() or -1
    local targetId = mq.TLO.Target.ID() or -1
    local maTargetId = mq.TLO.Me.GroupAssistTarget.ID() or -1
    local myLevel = mq.TLO.Me.Level() or 0

    local availableWidth, availableHeight = ImGui.GetContentRegionAvail()

    local numberOfEntriesInTable = #records
    local rowsToShow = math.max(numberOfEntriesInTable, numberOfEntriesInTable / 2)
    rowsToShow = math.min(rowsToShow, 12)

    tableHeight = tableHeight or (TEXT_BASE_HEIGHT * (rowsToShow + 3))

    if ImGui.BeginTable("ZoneBreakdown##"..tableKey, #columns, TABLE_FLAGS, -1.0, tableHeight, 0.0) then
        linq.from(columns)
            :orderBy(function(column) return column.order end)
            :foreach(function(index, column)
                ImGui.TableSetupColumn(column.label, column.flags, column.width, column.id)
             end)
        ImGui.TableSetupScrollFreeze(0, 1)

        local filteredRecords = linq.from(records)

        local sortSpecs = ImGui.TableGetSortSpecs()
        if sortSpecs and sortSpecs.SpecsDirty then
            for n = 1, sortSpecs.SpecsCount, 1 do
                local sortSpec = sortSpecs:Specs(n)
                local sortMethod = sortMethods[sortSpec.ColumnUserID]
                activeSorts[tableKey] = sortDelegate(sortMethod, sortSpec.SortDirection)
            end

            sortSpecs.SpecsDirty = false
        end

        filteredRecords = (
            (activeSorts[tableKey] and activeSorts[tableKey](filteredRecords)) -- Sort by selected sort filter
            or sortDelegate(sortMethods[COLUMN_ID.NAME], ImGuiSortDirection.Ascending)(filteredRecords) -- Sort by default sort filter (name)
        ):toArray()

        ImGui.TableHeadersRow()

        local clipper = ImGuiListClipper.new()
        clipper:Begin(#filteredRecords)
        while clipper:Step() do
            for rowNumber = clipper.DisplayStart, clipper.DisplayEnd - 1, 1 do
                local recordEntry = filteredRecords[rowNumber + 1]
                local record = utils.getCachedData(
                    string.format("cachedSpawnData-%s", recordEntry.id),
                    function() return recordEntry.spawnDataAccessor() end,
                    0.15
                )
                local recordIdentifier = string.format("%s##%s", record.name, record.id)
                ImGui.PushID(recordIdentifier)

                ImGui.TableNextRow()

                if record.id == myId then
                    ImGui.TableSetBgColor(ImGuiTableBgTarget.RowBg0, 1, 1, 1, 0.20)
                end

                if record.id == maTargetId then
                    ImGui.TableSetBgColor(ImGuiTableBgTarget.RowBg0, 1, 1, 0, 0.20)
                end

                if record.id == targetId then
                    ImGui.TableSetBgColor(ImGuiTableBgTarget.RowBg0, 0, 1, 1, 0.20)
                end

                ImGui.TableNextColumn()
                ImGui.Text(record.name)
                if ImGui.IsItemClicked() then
                    playerActionsService:TargetById(record.id)
                end
                uiComponents.ToolTip(string.format("Click to target %s", record.name))

                ImGui.TableNextColumn()
                if record.los then
                    ImGui.Text(ICON.FA_EYE)
                    uiComponents.ToolTip(string.format("%s is in line of sight", record.name))
                end

                ImGui.TableNextColumn()
                ImGui.Text(record.distance)

                ImGui.TableNextColumn()

                local conColorName = record.conColor or "BLACK"
                local conColorFieldName = conColorName:gsub(" ", ""):upper()
                local considerSign = CONSIDER_SIGN_LOOKUP[conColorFieldName]

                local levelDiff = (record.level or 0) - myLevel
                local levelDiffText = string.format((considerSign == 1 and "%d levels above") or (considerSign == 0 and "the same level as") or "%d levels below", math.abs(levelDiff))
                local levelTooltipText = string.format("%s is %s you and cons %s", record.name, levelDiffText, conColorName:lower())
                local conIcon = (considerSign == 1 and ICON.FA_ARROW_CIRCLE_UP) or (considerSign == 0 and ICON.FA_MINUS_CIRCLE) or ICON.FA_ARROW_CIRCLE_DOWN
                local contrastingColor = uiComponents.getContrastingColor(conColorFieldName)

                uiComponents.DrawWithBackground(function()
                    uiComponents.TextColored(conIcon, COLOR[conColorFieldName])
                end, contrastingColor, 15, 16, record.id, 1, 0)
                uiComponents.ToolTip(levelTooltipText)
                ImGui.SameLine()
                ImGui.Text(record.level)
                uiComponents.ToolTip(levelTooltipText)

                ImGui.TableNextColumn()
                ImGui.Text(record.race)
                ImGui.TableNextColumn()
                ImGui.Text(record.class)

                ImGui.TableNextColumn()
                uiComponents.DrawInfoBar((record.pctHps or 0) / 100, string.format("HP %d%%", record.pctHps), -1, 15)

                ImGui.TableNextColumn()
                if ImGui.SmallButton(record.location) then
                    navService:NavigateToLoc(record.location)
                end
                uiComponents.ToolTip(string.format("Click to navigate to %s @ %s", record.name, record.location))

                ImGui.PopID()
            end
        end

        ImGui.EndTable()
    end
end

local QuickHudZoneBreakoutWindow = function()
    if (not state.breakdownOpen) or (not mq.TLO.Me.CleanName()) then return end
    uiComponents.PushWindowSettingsStyles(state)
    local flags = (not state.settings.showTitleBar) and ImGuiWindowFlags.NoTitleBar or 0
    state.breakdownOpen, state.shouldDrawBreakdown = ImGui.Begin("QuickHudZoneBreakoutWindow", state.breakdownOpen, flags)
    if state.shouldDrawBreakdown then
        if ImGui.GetWindowHeight() == 32 and ImGui.GetWindowWidth() == 32 then
            ImGui.SetWindowSize(460, 177)
        end

        displayFullZoneBreakdown()
    end
    ImGui.End()
    uiComponents.PopWindowSettingsStyles()
end

mq.imgui.init('QuickHudZoneBreakoutWindow', QuickHudZoneBreakoutWindow)
