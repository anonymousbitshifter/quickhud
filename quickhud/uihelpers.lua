local ICON = require('quickhud.lib.icons')
local utils = require('quickhud.utils')
local COLOR = require('quickhud.lib.color')

local log = require('quickhud.lib.Write')

local uihelpers = {}

uihelpers.PushWindowSettingsStyles = function(state)
    local style = ImGui.GetStyle()
    ImGui.PushStyleVar(ImGuiStyleVar.FramePadding, ImVec2.new(style.FramePadding.x, style.FramePadding.y * (state.settings.paddingMultiplier or 1)))
    ImGui.PushStyleVar(ImGuiStyleVar.ItemSpacing, ImVec2.new(style.ItemSpacing.x, style.ItemSpacing.y * (state.settings.paddingMultiplier or 1)))
    ImGui.PushStyleVar(ImGuiStyleVar.Alpha, state.settings.contentOpacity)
    ImGui.PushStyleVar(ImGuiStyleVar.FrameRounding, 3)
    ImGui.PushStyleVar(ImGuiStyleVar.PopupRounding, 5)
    ImGui.PushStyleVar(ImGuiStyleVar.GrabRounding, 3)
    ImGui.PushStyleColor(ImGuiCol.WindowBg, state.settings.backgroundColor[1], state.settings.backgroundColor[2], state.settings.backgroundColor[3], state.settings.backgroundColor[4] or 1)
    ImGui.PushStyleColor(ImGuiCol.Text, state.settings.textColor[1], state.settings.textColor[2], state.settings.textColor[3], state.settings.textColor[4] or 1)
    ImGui.PushStyleColor(ImGuiCol.Separator, state.settings.separatorColor[1], state.settings.separatorColor[2], state.settings.separatorColor[3], state.settings.separatorColor[4] or 1)
    ImGui.SetNextWindowBgAlpha(state.settings.windowOpacity)
end

uihelpers.PopWindowSettingsStyles = function()
    ImGui.SetNextWindowBgAlpha(1)
    ImGui.PopStyleColor(3)
    ImGui.PopStyleVar(6)
end

uihelpers.NextColumn = function(state)
    ImGui.PushStyleColor(ImGuiCol.Separator, 1, 0, 0, 1)
    ImGui.PushStyleColor(ImGuiCol.SeparatorHovered, state.settings.textColor[1], state.settings.textColor[2], state.settings.textColor[3], state.settings.textColor[4] or 1)
    ImGui.PushStyleColor(ImGuiCol.SeparatorActive, state.settings.textColor[1], state.settings.textColor[2], state.settings.textColor[3], state.settings.textColor[4] or 1)
    ImGui.NextColumn()
    ImGui.PopStyleColor(3)
end

uihelpers.getContrastingColor = function(colorName)
    return (colorName == "WHITE" or colorName == "YELLOW" or colorName == "LIGHTBLUE") and COLOR.BLACK or COLOR.WHITE
end

uihelpers.TextColored = function(text, textColor)
    if (nil == textColor) or (nil == next(textColor)) then
        textColor = {1,1,1,1}
    end

    ImGui.TextColored(textColor[1] or 1, textColor[2] or 1, textColor[3] or 1, textColor[4] or 1, text)
end

uihelpers.DisabledButton = function(text)
    ImGui.PushStyleColor(ImGuiCol.Button, 1,1,1,0.5)
    ImGui.PushStyleColor(ImGuiCol.ButtonHovered, 1,1,1,0.5)
    ImGui.PushStyleColor(ImGuiCol.ButtonActive, 1,1,1,0.5)
    local clicked = ImGui.Button(text)
    ImGui.PopStyleColor(3)
    return clicked
end

uihelpers.ButtonColored = function(text, buttonColor, small, width, height, hoveredColor, activeColor, textColor)
    local clicked = false
    local renderButtonMethod = small and ImGui.SmallButton or ImGui.Button
    if not buttonColor and not hoveredColor and not activeColor and not textColor then
        clicked = renderButtonMethod(text)
    else
        buttonColor = buttonColor or {0,0,0,0.5}
        hoveredColor = hoveredColor or buttonColor
        activeColor = activeColor or buttonColor
        textColor = textColor or {1,1,1,1}
        ImGui.PushStyleColor(ImGuiCol.Button, buttonColor[1], buttonColor[2], buttonColor[3], buttonColor[4] or 1)
        ImGui.PushStyleColor(ImGuiCol.ButtonHovered, hoveredColor[1], hoveredColor[2], hoveredColor[3], hoveredColor[4] or 1)
        ImGui.PushStyleColor(ImGuiCol.ButtonActive, activeColor[1], activeColor[2], activeColor[3], activeColor[4] or 1)
        ImGui.PushStyleColor(ImGuiCol.Text, textColor[1], textColor[2], textColor[3], textColor[4] or 1)
        if width and height then
            clicked = renderButtonMethod(text, width, height)
        else
            clicked = renderButtonMethod(text)
        end
        ImGui.PopStyleColor(4)
    end

    return clicked
end

uihelpers.SmallButtonColored = function(text, buttonColor, hoveredColor, activeColor, textColor)
    buttonColor = buttonColor or {0,0,0,0.5}
    hoveredColor = hoveredColor or buttonColor
    activeColor = activeColor or buttonColor
    textColor = textColor or {1,1,1,1}
    ImGui.PushStyleColor(ImGuiCol.Button, buttonColor[1], buttonColor[2], buttonColor[3], buttonColor[4] or 1)
    ImGui.PushStyleColor(ImGuiCol.ButtonHovered, hoveredColor[1], hoveredColor[2], hoveredColor[3], hoveredColor[4] or 1)
    ImGui.PushStyleColor(ImGuiCol.ButtonActive, activeColor[1], activeColor[2], activeColor[3], activeColor[4] or 1)
    ImGui.PushStyleColor(ImGuiCol.Text, textColor[1], textColor[2], textColor[3], textColor[4] or 1)
    local clicked = ImGui.SmallButton(text)
    ImGui.PopStyleColor(4)
    return clicked
end

function moveElement(list, entry, oldIndex, newIndex)
    table.remove(list, oldIndex)
    -- log.Debug(string.format("moving '%s' %d to %d out of %d", entry, oldIndex, newIndex, max))
    table.insert(list, newIndex, entry)
end

uihelpers.ReorderableList = function(list, renderer, moveFunction)
    moveFunction = moveFunction or moveElement
    local max = #list
    local newOrder = utils.copyTable(list)

    for i, entry in ipairs(list) do
        if i > 1 then
            if ImGui.SmallButton(string.format("%s##%s", ICON.FA_CHEVRON_UP, i)) then
                local newIndex = utils.clamp(i - 1, 1, max)
                moveFunction(newOrder, entry, i, newIndex)
            end
            uihelpers.ToolTip("Move up")
        else
            uihelpers.DisabledSmallButton(ICON.FA_CHEVRON_UP)
        end
        ImGui.SameLine()
        if i < max then
            if ImGui.SmallButton(string.format("%s##%s", ICON.FA_CHEVRON_DOWN, i)) then
                local newIndex = utils.clamp(i + 1, 1, max)
                moveFunction(newOrder, entry, i, newIndex)
            end
            uihelpers.ToolTip("Move down")
        else
            uihelpers.DisabledSmallButton(ICON.FA_CHEVRON_DOWN)
        end
        ImGui.SameLine()
        if type(renderer) == "function" then
            renderer(i, entry)
        else
            ImGui.Text(tostring(i)..": "..tostring(renderer or "List Item"))
        end
    end

    return newOrder
end

uihelpers.DisabledSmallButton = function(text)
    ImGui.PushStyleColor(ImGuiCol.Button, 1,1,1,0.5)
    ImGui.PushStyleColor(ImGuiCol.ButtonHovered, 1,1,1,0.5)
    ImGui.PushStyleColor(ImGuiCol.ButtonActive, 1,1,1,0.5)
    local clicked = ImGui.SmallButton(text)
    ImGui.PopStyleColor(3)
    return clicked
end

uihelpers.ToolTip = function(desc)
    if ImGui.IsItemHovered() then
        ImGui.BeginTooltip()
        if type(desc) == 'function' then
            desc()
        else
            ImGui.PushTextWrapPos(ImGui.GetFontSize() * 35.0)
            ImGui.Text(desc)
            ImGui.PopTextWrapPos()
        end
        ImGui.EndTooltip()
    end
end

uihelpers.HelpMarker = function(desc)
    ImGui.TextColored(0.15, 0.55, 0.75, 1, ICON.MD_HELP_OUTLINE)
    uihelpers.ToolTip(desc)
end

uihelpers.DrawComboBox = function(label, resultvar, options, bykey)
    if ImGui.BeginCombo(label, resultvar) then
        for i,j in pairs(options) do
            if bykey then
                if ImGui.Selectable(i, i == resultvar) then
                    resultvar = i
                end
            else
                if ImGui.Selectable(j, j == resultvar) then
                    resultvar = j
                end
            end
        end
        ImGui.EndCombo()
    end
    return resultvar
end

uihelpers.DrawToggleButton = function(labelText, idText, resultVar, helpText)
    ImGui.SetWindowFontScale(1.75)
    local originalYPos = ImGui.GetCursorPosY()

    ImGui.SetCursorPosY(ImGui.GetCursorPosY() - 5)
    if resultVar then
        uihelpers.TextColored(ICON.FA_TOGGLE_ON, COLOR.STEELBLUE)
    else
        ImGui.Text(ICON.FA_TOGGLE_OFF)
    end
    uihelpers.ToolTip(string.format("Click to turn %s", resultVar and "off" or "on"))

    if ImGui.IsItemClicked() then
        resultVar = not resultVar
    end

    ImGui.SetWindowFontScale(1.0)

    ImGui.SameLine()

    ImGui.SetCursorPosY(originalYPos)
    ImGui.Text(labelText)

    if helpText then
        ImGui.SameLine()
        uihelpers.HelpMarker(helpText)
    end

    return resultVar
end

uihelpers.DrawCheckBox = function(labelText, idText, resultVar, helpText)
    resultVar,_ = ImGui.Checkbox(idText, resultVar)
    ImGui.SameLine()
    ImGui.Text(labelText)
    if helpText then
        ImGui.SameLine()
        uihelpers.HelpMarker(helpText)
    end
    return resultVar
end

uihelpers.GetAvailableX = function()
    local availX, availY = ImGui.GetContentRegionAvail()
    return availX
end

uihelpers.GetAvailableY = function()
    local availX, availY = ImGui.GetContentRegionAvail()
    return availY
end

uihelpers.OffsetCursorPosX = function(offsetAmount)
    local xPos = ImGui.GetCursorPosX()
    ImGui.SetCursorPosX(xPos + offsetAmount)
end

uihelpers.OffsetCursorPosY = function(offsetAmount)
    local yPos = ImGui.GetCursorPosY()
    ImGui.SetCursorPosY(yPos + offsetAmount)
end

uihelpers.DrawColorEditor = function(label, resultVar)
    local col, _ = ImGui.ColorEdit3(label, resultVar, ImGuiColorEditFlags.NoInputs)
    if col then
        resultVar = col
    end
    return resultVar
end

uihelpers.DrawWithBackground = function(content, backgroundColor, width, height, key, paddingX, paddingY)
    if (nil == backgroundColor) or (nil == next(backgroundColor)) then
        backgroundColor = {0,0,0,1}
    end
    ImGui.PushStyleVar(ImGuiStyleVar.FramePadding, paddingX or 5, paddingY or 4)
    ImGui.PushStyleColor(ImGuiCol.FrameBg, backgroundColor[1] or 1, backgroundColor[2] or 1, backgroundColor[3] or 1, backgroundColor[4] or 1)
    ImGui.BeginChildFrame(key, width or -1, height or -1, ImGuiWindowFlags.NoScrollbar)
    if type(content) == 'function' then
        content()
    else
        ImGui.Text(content)
    end
    ImGui.AlignTextToFramePadding()
    ImGui.EndChildFrame()
    ImGui.PopStyleColor()
    ImGui.PopStyleVar()
end

uihelpers.DrawInfoBar = function(ratio, text, width, height, barColor)
    height = height or 20
    width = width or -1

    if nil ~= barColor then
        ImGui.PushStyleColor(ImGuiCol.PlotHistogram, barColor[1] or 0, barColor[2] or 0, barColor[3] or 0, barColor[4] or 1)
    else
        ImGui.PushStyleColor(ImGuiCol.PlotHistogram, 1 - ratio, ratio, 0, 0.5)
    end

    ImGui.PushStyleColor(ImGuiCol.Text, 1, 1, 1, 1)
    ImGui.ProgressBar(ratio, width, height, text)
    ImGui.PopStyleColor(2)
end

local searchText = ""

uihelpers.DrawIconChooser = function(selectedIcon, label)
    if ImGui.Button(selectedIcon) then
        ImGui.SetNextWindowPos(ImGui.GetCursorScreenPos())
        ImGui.SetNextWindowSize(325,250)
        ImGui.OpenPopup("Icon Chooser")
    end
    ImGui.SameLine()
    ImGui.Text(label)

    local wrapCount = 8
    local placedCount = 0

    if ImGui.BeginPopupModal("Icon Chooser", true, ImGuiWindowFlags.NoMove + ImGuiWindowFlags.NoResize) then
        searchText = ImGui.InputText("Filter Icons", searchText, 100)
        searchText = utils.trim(searchText)

        for key, value in pairs(ICON) do
            if string.len(searchText) <= 0 or string.match(key, searchText) then
                if placedCount > 0 and placedCount <= wrapCount then
                    ImGui.SameLine()
                elseif placedCount > wrapCount then
                    placedCount = 0
                end

                local iconClicked = ImGui.Button(value, 25, 25)

                if ImGui.IsItemHovered() then
                    ImGui.BeginTooltip()
                    ImGui.PushTextWrapPos(ImGui.GetFontSize() * 35.0)
                    ImGui.Text(key)
                    ImGui.PopTextWrapPos()
                    ImGui.EndTooltip()
                end

                if iconClicked then
                    ImGui.CloseCurrentPopup()
                    searchText = ""
                    selectedIcon = value
                end

                placedCount = placedCount + 1
            end
        end
        ImGui.EndPopup()
    end

    return selectedIcon
end

local uiElementKeys = {}

local function getUIElementKey(key)
    local elementKey = uiElementKeys[key]

    if not elementKey then
        elementKey = math.random(9999999, 1000000)
        uiElementKeys[key] = elementKey
    end

    return elementKey
end
uihelpers.getUIElementKey = getUIElementKey

function patchTable(args, defaults)
    for key, value in pairs(defaults) do
        if not args[key] then
            args[key] = value
        end
    end

    return args
end

uihelpers.renderWithBackground = function(content, args)
    local style = ImGui.GetStyle()
    args = patchTable(args, {
        backgroundColor = {0, 0, 0, 1},
        width = -1,
        height = -1,
        paddingX = 0,
        paddingY = 0,
        rounding = style.FrameRounding,
        alpha = 1
    })

    local xPos = ImGui.GetCursorPosX()
    local yPos = ImGui.GetCursorPosY()

    ImGui.PushStyleColor(ImGuiCol.PlotHistogram, args.backgroundColor[1] or 0, args.backgroundColor[2] or 0, args.backgroundColor[3] or 0, args.backgroundColor[4] or args.alpha)
    ImGui.PushStyleVar(ImGuiStyleVar.FrameRounding, args.rounding)
    ImGui.ProgressBar(1.0, args.width, args.height, " ")
    ImGui.PopStyleColor()

    ImGui.SetCursorPosX(xPos + args.paddingX)
    ImGui.SetCursorPosY(yPos + args.paddingY)

    if type(content) == "function" then
        content()
    else
        ImGui.Text(content)
    end

    -- ImGui.SetCursorPosY(yPos)

    ImGui.PopStyleVar(1)
end

local panels = {}

local function StartWrapPanel(itemSize)
    local style = ImGui.GetStyle()

    local panel = {
        lineIndex = 1,
        rowIndex = 1,
        columns = math.floor(uihelpers.GetAvailableX() / (itemSize + (style.FramePadding.x * 2)))
    }

    table.insert(panels, panel)
end
uihelpers.StartWrapPanel = StartWrapPanel

local function NextWrapPanelItem()
    local panel = panels[#panels]

    if panel.lineIndex > (panel.columns * panel.rowIndex) then
        panel.rowIndex = panel.rowIndex + 1
    elseif panel.lineIndex > 1 then
        ImGui.SameLine()
        -- uihelpers.renderWithBackground("Q",{
        --     backgroundColor = COLOR.RED,
        --     width = TEXT_BASE_WIDTH,
        --     height = TEXT_BASE_HEIGHT / 2,
        --     paddingX = 0,
        --     paddingY = 0,
        --     rounding = 0
        -- })
        -- ImGui.SameLine()
    end

    panel.lineIndex = panel.lineIndex + 1
end
uihelpers.NextWrapPanelItem = NextWrapPanelItem

local function EndWrapPanel()
    table.remove(panels)
end
uihelpers.EndWrapPanel = EndWrapPanel


return uihelpers
