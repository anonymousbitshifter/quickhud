local state = {}

state.settings = {}
state.settingsBuffer = nil
state.settingsComponents = {}

state.openGUI = true
state.shouldDrawGUI = true
state.openSettingsUI = false
state.shouldDrawSettingsUI = false

state.windowX = 400
state.windowY = 400
state.windowWidth = 400
state.windowHeight = 200

state.breakdownOpen = false
state.shouldDrawBreakdown = false

state.locked = false

return state
