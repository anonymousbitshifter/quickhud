--- @type mq
local mq = require 'mq'

local log = require('quickhud.lib.Write')

-- Polyfills

table.pack = table.pack or function(...) return { n = select("#", ...), ... } end
table.unpack = table.unpack or unpack

-- General utilities

local utils = {}

utils.formatStatValue = function(statValue)
    statValue = statValue or 0
    if statValue >= 1000000 then
        return string.format("%.3f", statValue/1000000).."m"
    elseif statValue >= 100000 then
        return string.format("%d", statValue/1000).."k"
    elseif statValue >= 10000 then
        return string.format("%.1f", statValue/1000).."k"
    else
        return statValue
    end
end

local cache = {}

utils.noop = function() end

utils.removeCachedItem = function(key)
    cache[key] = nil
end

utils.fileExists = function(path)
    local f = io.open(path, "r")
    if f ~= nil then io.close(f) return true else return false end
end

utils.getCachedData = function(key, accessor, cacheDuration)
    cacheDuration = cacheDuration or 5
    if nil == cache[key] or (os.clock() - cache[key].timestamp) >= cacheDuration then
        local data = accessor()
        cache[key] = { value = data, timestamp = os.clock() }
        return data
    end

    return cache[key].value
end

utils.spairs = function(t, order)
    -- collect the keys
    local keys = {}
    for k in pairs(t) do keys[#keys+1] = k end

    -- if order function given, sort by it by passing the table and keys a, b,
    -- otherwise just sort the keys
    if order then
        table.sort(keys, function(a,b) return order(t, a, b) end)
    else
        table.sort(keys)
    end

    -- return the iterator function
    local i = 0
    return function()
        i = i + 1
        if keys[i] then
            return keys[i], t[keys[i]]
        end
    end
end

utils.set = function(list)
    local set = {}
    for _, l in ipairs(list) do set[l] = true end
    return set
end

utils.split = function(inputstr, sep)
    if sep == nil then
            sep = "%s"
    end
    local t={}
    for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
            table.insert(t, str)
    end
    return t
end

utils.trim = function(s)
    return (string.gsub(s, "^%s*(.-)%s*$", "%1"))
end

utils.hex = function(hex, alpha)
	local redColor,greenColor,blueColor=hex:match(((string.len(hex) <= 4) and '#?(.)(.)(.)') or '#?(..)(..)(..)')
	redColor, greenColor, blueColor = tonumber(redColor, 16)/255, tonumber(greenColor, 16)/255, tonumber(blueColor, 16)/255
	redColor, greenColor, blueColor = math.floor(redColor*100)/100, math.floor(greenColor*100)/100, math.floor(blueColor*100)/100
	if alpha == nil then
		return redColor, greenColor, blueColor
	end
	return redColor, greenColor, blueColor, alpha
end

utils.rgb = function(r, g, b)
	local redColor,greenColor,blueColor=r/255, g/255, b/255
	redColor, greenColor, blueColor = math.floor(redColor*100)/100, math.floor(greenColor*100)/100, math.floor(blueColor*100)/100
	return redColor, greenColor, blueColor
end

utils.rgba = function(r, g, b, alpha)
	local redColor,greenColor,blueColor=r/255, g/255, b/255
	redColor, greenColor, blueColor = math.floor(redColor*100)/100, math.floor(greenColor*100)/100, math.floor(blueColor*100)/100
	return redColor, greenColor, blueColor, alpha
end

local debounceTable = {}
utils.debounce = function(method, key, window)
    key = key or "anonymous"
    window = window or 1
    if not debounceTable[key] or os.difftime(os.time(), debounceTable[key]) >= window then
        method()
        debounceTable[key] = os.time()
    end
end

utils.createDirectory = function(directoryPath)
    os.execute('mkdir "'..directoryPath:gsub('/','\\')..'" 1>nul: 2>&1')
end

utils.tableConcat = function(t1, t2)
    local t = {}
    for k,v in ipairs(t1) do
        table.insert(t, v)
    end
    for k,v in ipairs(t2) do
        table.insert(t, v)
    end
    return t
end

utils.scandir = function(directory)
    local i, t, popen = 0, {}, io.popen
    for filename in popen('dir "'..directory..'" /b'):lines() do
        i = i + 1
        t[i] = filename
    end
    return t
end

utils.deleteFile = function(path)
    os.remove(path)
end

utils.uuid = function()
    math.randomseed(os.clock()*math.random(15020209090,95020209090))
    for i=1,6 do
        math.random(10000, 65000)
    end

    local template ='xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'
    return string.gsub(template, '[xy]', function (c)
        local v = (c == 'x') and math.random(0, 0xf) or math.random(8, 0xb)
        return string.format('%x', v)
    end)
end

utils.copyFile = function(source, dest)
    local f = io.open(source, 'r')
    local contents = f:read('*a')
    io.close(f)
    f = io.open(dest, 'w')
    f:write(contents)
    io.close(f)
end

utils.safeCall = function(fn, key, arguments)
    key = key or "anonymous"
    local success, result = pcall(fn, arguments and unpack(arguments) or nil)
    if not success then
        utils.debounce(function()
            log.Error(string.format("An error occurred during the procedure. The error will be logged. [key=%s]", key))
            log.Error(result)
        end, key)
    end
    return success, result
end

utils.copyTable = function(obj, seen)
    if type(obj) ~= 'table' then return obj end
    if seen and seen[obj] then return seen[obj] end
    local s = seen or {}
    local res = setmetatable({}, getmetatable(obj))
    s[obj] = res
    for k, v in pairs(obj) do res[utils.copyTable(k, s)] = utils.copyTable(v, s) end
    return res
end

utils.findIndex = function(fromTable, predicate)
    for index, value in ipairs(fromTable) do
        if type(predicate) == 'function' and predicate(value) then
            return index
        elseif value == predicate then
            return index
        end
    end
    return nil
end

utils.clamp = function(numberToClamp, min, max)
    return math.min(math.max(numberToClamp, min), max)
end

local intHashCache = {}

utils.buildIntHash = function(text)
    if intHashCache[text] then return intHashCache[text] end
    local hashTotal = 0
    for i = 1, string.len(text) do
        hashTotal = hashTotal + string.byte(text, i)
    end
    intHashCache[text] = hashTotal
    return hashTotal
end

return utils
