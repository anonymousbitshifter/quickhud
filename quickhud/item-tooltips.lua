-- TODO: loki - Make right click or something open the item in a dedicated window ala the regular UI
--             The window can have tabs or whatever for additional data and comparisons

--- @type Mq
local mq = require('mq')

--- @type ImGui
require('ImGui')

local ICON = require('quickhud.lib.icons')

-- EQ Texture Animation references
local animItems = mq.FindTextureAnimation("A_DragItem")
local animBox = mq.FindTextureAnimation("A_RecessedBox")

local debug = false

local TEXT_BASE_WIDTH, _ = ImGui.CalcTextSize("A")
local TEXT_BASE_HEIGHT = ImGui.GetTextLineHeightWithSpacing();

local WORN_SLOT_NAMES = { "Charm", "Left Ear", "Head", "Face", "Right Ear", "Neck", "Shoulder", "Arms", "Back", "Left Wrist", "Right Wrist", "Ranged", "Hands", "Main Hand", "Off Hand", "Left Finger", "Right Finger", "Chest", "Legs", "Feet", "Waist", "Power Source", "Ammo" }
local SLOT_NAMES = { "Charm", "Left Ear", "Head", "Face", "Right Ear", "Neck", "Shoulder", "Arms", "Back", "Left Wrist", "Right Wrist", "Ranged", "Hands", "Main Hand", "Off Hand", "Left Finger", "Right Finger", "Chest", "Legs", "Feet", "Waist", "Power Source", "Ammo", "Bag 1", "Bag 2", "Bag 3", "Bag 4", "Bag 5", "Bag 6", "Bag 7", "Bag 8", "Bag 9", "Bag 10" }

local COLOR = require('quickhud.lib.color')

local ADDITIONAL_ATTRIBUTES = {
   { meta = { prefix = "Heroic" }, color = COLOR.GOLDENROD },
   { meta = { postfix = "Regen" }, color = COLOR.LIMEGREEN }
}

local BASE_ATTRIBUTES = { "AC", "HP", "Mana", "Endurance", "Purity", "Haste" }
local ATTRIBUTES = { "AGI", "DEX", "STA", "STR", "INT", "WIS", "CHA" }
local ADVANCED_ATTRIBUTES = { "Attack", "HealAmount", "SpellDamage", "SpellShield", "Clairvoyance", "DamShield", "InstrumentMod" }
local SAVES = { "svCold", "svCorruption", "svDisease", "svFire", "svMagic", "svPoison" }
local MINOR_ATTRIBUTES = { "Weight", "Size", "Tribute" }

local ATTRIBUTE_TRANSLATIONS = {
   AC = "Armor Class",
   HP = "Hitpoints",
   Mana = "Mana",
   Endurance = "Endurance",
   Attack = "Attack",
   AGI = "Agility",
   DEX = "Dexterity",
   STA = "Stamina",
   STR = "Strength",
   INT = "Intelligence",
   WIS = "Wisdom",
   CHA = "Charisma",
   Clairvoyance = "Clairvoyance",
   DamShield = "DamShield",
   Haste = "Haste",
   svCold = "Cold",
   svCorruption = "Corruption",
   svDisease = "Disease",
   svFire = "Fire",
   svMagic = "Magic",
   svPoison = "Poison"
}

local uiElementKeys = {}

local function getUIElementKey(key)
    local elementKey = uiElementKeys[key]

    if not elementKey then
        elementKey = math.random(9999999, 1000000)
        uiElementKeys[key] = elementKey
    end

    return elementKey
end

local function getAttributeLabel(attribute)
   return ATTRIBUTE_TRANSLATIONS[attribute] or attribute
end

local function getAdditionalAttributeValue(item, attribute, additionalAttributeMeta)
   local additionalValue = item[(additionalAttributeMeta.prefix or "") .. attribute .. (additionalAttributeMeta.postfix or "")]
   additionalValue = additionalValue and additionalValue() or nil
   if additionalValue and additionalValue > 0 then
      return additionalValue
   end
end

local function getAttributes(item, attributes)
   local populatedAttributes = {}
   for index, attribute in ipairs(attributes) do
      local attributeValue = item[attribute]()
      if attributeValue and attributeValue > 0 then
         local data = {
            attribute = attribute,
            value = attributeValue,
            additionalValues = {}
         }

         for additionalIndex, additionalAttributeEntry in ipairs(ADDITIONAL_ATTRIBUTES) do
            local additionalValue = getAdditionalAttributeValue(item, attribute, additionalAttributeEntry.meta)
            if additionalValue then
               local additionalInfo = {
                  meta = additionalAttributeEntry.meta,
                  color = additionalAttributeEntry.color,
                  value = additionalValue
               }
               table.insert(data.additionalValues, additionalInfo)
            end
         end

         table.insert(populatedAttributes, data)
      end
   end
   return populatedAttributes
end

local function renderItemIcon(item, boxSize, paddingX, paddingY)
   paddingX = paddingX or 2
   paddingY = paddingY or 2
   local iconSize = boxSize - (paddingX + paddingY)

   local yPos = ImGui.GetCursorPosY()

   ImGui.BeginGroup()

   local xPos = ImGui.GetCursorPosX()
   ImGui.DrawTextureAnimation(animBox, boxSize, boxSize)
   ImGui.SameLine()
   ImGui.SetCursorPosX(xPos + paddingX)
   ImGui.SetCursorPosY(yPos + paddingY)

   animItems:SetTextureCell(item.Icon() - 500)
   ImGui.DrawTextureAnimation(animItems, iconSize, iconSize)

   ImGui.EndGroup()
end

local function renderAttributes(item, attributeValues, useAttributeValueColors)
    ImGui.BeginGroup()

    ImGui.BeginGroup()
    for index, attribute in ipairs(attributeValues) do
        ImGui.Text(string.format("%s:", getAttributeLabel(attribute.attribute)))
    end
    ImGui.EndGroup()

    ImGui.SameLine()
    ImGui.Dummy(TEXT_BASE_WIDTH * 0.5, TEXT_BASE_HEIGHT)
    ImGui.SameLine()

    ImGui.BeginGroup()

    for index, attribute in ipairs(attributeValues) do
        local color = COLOR.WHITE
        if useAttributeValueColors then
            color = ((attribute.value > 0) and COLOR.LIMEGREEN) or ((attribute.value < 0) and COLOR.ORANGERED) or COLOR.WHITE
        end
        local format = "%s"
        local integer, fraction = math.modf(attribute.value)
        if fraction > 0 then
            format = "%.2f"
        end
        if debug then
            ImGui.Text(string.format(format .. " (%s)", getAttributeLabel(attribute.value), attribute.attribute))
        else
            ImGui.TextColored(color[1], color[2], color[3], color[4] or 1, string.format(format, getAttributeLabel(attribute.value)))
        end
    end
    ImGui.EndGroup()

    ImGui.SameLine()

    ImGui.BeginGroup()
    for index, attribute in ipairs(attributeValues) do
        if #attribute.additionalValues > 0 then
            for additionalIndex, additionalAttribute in ipairs(attribute.additionalValues) do
                local color = additionalAttribute.color
                local meta = additionalAttribute.meta

                local format = " +%s %s"
                local integer, fraction = math.modf(attribute.value)
                if fraction > 0 then
                    format = " +%.2f %s"
                end

                if debug then
                    ImGui.TextColored(color[1], color[2], color[3], color[4] or 1, string.format(format .. " (%s)", additionalAttribute.value, meta.prefix or meta.postfix, attribute.attribute))
                else
                    ImGui.TextColored(color[1], color[2], color[3], color[4] or 1, string.format(format, additionalAttribute.value, meta.prefix or meta.postfix))
                end
            end
        else
            if debug then
                ImGui.Text(string.format(" +(%s)", attribute.attribute))
            else
                ImGui.Dummy(TEXT_BASE_WIDTH, TEXT_BASE_HEIGHT * 0.8)
            end
        end
    end
    ImGui.EndGroup()

    ImGui.EndGroup()
end

local function compareItemAttributes(item1, item2)

end

local function isWearable(item)
   local wearableTypes = {
      ["Armor"] = true,
      ["Jewelry"] = true,
      ["1H Slashing"] = true,
      ["1H Blunt"] = true,
      ["1H Piercing"] = true,
      ["2H Slashing"] = true,
      ["2H Blunt"] = true,
      ["2H Piercing"] = true,
      ["Charm"] = true
   }

   return wearableTypes[item.Type()]
end

local function getWornSlots(item)
   local itemSlots = {}
   for index = 1, item.WornSlots() do
      local wornSlot = item.WornSlot(index)()
      if wornSlot then
         table.insert(itemSlots, wornSlot)
      end
   end
   return itemSlots
end

local function renderWornSlots(item)
   local wornSlots = getWornSlots(item)
   local itemSlots = {}
   for index = 1, #wornSlots do
      local wornSlot = wornSlots[index]
      table.insert(itemSlots, string.format("%s (%s)", WORN_SLOT_NAMES[wornSlot + 1], wornSlot))
   end
   if itemSlots[1] then
      ImGui.PushTextWrapPos(ImGui.GetCursorPosX() + (ImGui.GetFontSize() * 15.0))
      ImGui.Text(table.concat(itemSlots, ", "))
      ImGui.PopTextWrapPos()
   end
end

local function renderWithBackground(content, backgroundColor, width, height, key, paddingX, paddingY)
   if (nil == backgroundColor) or (nil == next(backgroundColor)) then
       backgroundColor = {0,0,0,1}
   end
   ImGui.PushStyleVar(ImGuiStyleVar.FramePadding, paddingX or 5, paddingY or 2)
   ImGui.PushStyleColor(ImGuiCol.FrameBg, backgroundColor[1] or 1, backgroundColor[2] or 1, backgroundColor[3] or 1, backgroundColor[4] or 1)
   ImGui.BeginChildFrame(key, width or -1, height or -1, ImGuiWindowFlags.NoScrollbar)
   if type(content) == 'function' then
       content()
   else
       ImGui.Text(content)
   end
   ImGui.AlignTextToFramePadding()
   ImGui.EndChildFrame()
   ImGui.PopStyleColor()
   ImGui.PopStyleVar()
end

local function renderTextWithBackground(text, backgroundColor, key, textColor, paddingX, paddingY, width, height)
   renderWithBackground(
      text,
      backgroundColor,
      ImGui.CalcTextSize(text) + TEXT_BASE_WIDTH,
      TEXT_BASE_HEIGHT,
      getUIElementKey(key),
      paddingX,
      paddingY
   )
end

local function renderItemAttributes(item)
   ImGui.BeginGroup()

   local attributesHaveBeenRendered = false

   for index, attributes in ipairs({BASE_ATTRIBUTES, ATTRIBUTES, MINOR_ATTRIBUTES}) do
      local attributeValues = getAttributes(item, attributes)
      if #attributeValues > 0 then
         if attributesHaveBeenRendered then
            ImGui.Dummy(TEXT_BASE_WIDTH, TEXT_BASE_HEIGHT)
         end
         renderAttributes(item, attributeValues)
         attributesHaveBeenRendered = true
      end
   end

   ImGui.EndGroup()

   ImGui.SameLine()
   ImGui.Dummy(TEXT_BASE_WIDTH, TEXT_BASE_HEIGHT)
   ImGui.SameLine()

   attributesHaveBeenRendered = false

   ImGui.BeginGroup()
   for index, attributes in ipairs({ADVANCED_ATTRIBUTES, SAVES}) do
      local attributeValues = getAttributes(item, attributes)
      if #attributeValues > 0 then
         if attributesHaveBeenRendered then
            ImGui.Dummy(TEXT_BASE_WIDTH, TEXT_BASE_HEIGHT)
         end
         renderAttributes(item, attributeValues)
         attributesHaveBeenRendered = true
      end
   end
   ImGui.EndGroup()
end

local function renderItemDetails(item)
   local itemIdentifier = string.format("%s-%s-%s", item.ID(), item.Name(), item.ItemSlot())

   ImGui.BeginGroup()

   ImGui.BeginGroup()
   local slotName = SLOT_NAMES[item.ItemSlot() + 1]
   if WORN_SLOT_NAMES[item.ItemSlot() + 1] then
      renderTextWithBackground("Equipped", COLOR.DARKSLATEGRAY, itemIdentifier .. "-equipped-badge")
      ImGui.SameLine()
   else
      renderTextWithBackground("Inventory", COLOR.DARKSLATEGRAY, itemIdentifier .. "-intentory-badge")
      ImGui.SameLine()
   end
   if not WORN_SLOT_NAMES[item.ItemSlot() + 1] then
      slotName = string.format("%s (%s), Slot %s", mq.TLO.Me.Inventory(item.ItemSlot()).Name(), slotName, item.ItemSlot2() + 1)
   end
   renderTextWithBackground(slotName, COLOR.DARKSLATEGRAY, itemIdentifier .. "-slot-badge")
   ImGui.EndGroup()
   ImGui.Dummy(TEXT_BASE_WIDTH, TEXT_BASE_HEIGHT / 3)

   ImGui.BeginGroup()
   renderItemIcon(item, 50)
   ImGui.SameLine()
   ImGui.Dummy(5)
   ImGui.SameLine()
   ImGui.EndGroup()
   ImGui.SameLine()

   ImGui.BeginGroup()
   renderTextWithBackground(string.format("%s", item.Name(), item.ID()), COLOR.INDIGO, itemIdentifier .. "-name-badge")
   renderTextWithBackground(item.Type(), COLOR.OLIVE, itemIdentifier .. "-type-badge")

   ImGui.BeginGroup()
   local properties = { "Lore", "Attuneable", "NoDrop", "NoRent" }
   local renderedProperty = false
   for propertyIndex, property in ipairs(properties) do
      if item[property]() then
         if renderedProperty then ImGui.SameLine() end
         renderTextWithBackground(property, COLOR.DARKERGRAY, string.format("%s-property-badge-%s", itemIdentifier, propertyIndex))
         renderedProperty = true
      end
   end
   ImGui.EndGroup()

   renderWornSlots(item)

   if item.RequiredLevel() > 0 then
      ImGui.Text("Required Level: ")
      ImGui.SameLine()
      if item.RequiredLevel() <= mq.TLO.Me.Level() then
         ImGui.TextColored(COLOR.LIMEGREEN[1],COLOR.LIMEGREEN[2],COLOR.LIMEGREEN[3],COLOR.LIMEGREEN[4] or 1, item.RequiredLevel())
      else
         ImGui.TextColored(COLOR.ORANGERED[1],COLOR.ORANGERED[2],COLOR.ORANGERED[3],COLOR.ORANGERED[4] or 1, item.RequiredLevel())
      end
   end

   if item.SellPrice() then
      ImGui.Text(string.format("Sell Price: %s", item.SellPrice()))
   end
   if item.Stackable() then
      ImGui.Text(string.format("Stacked x%d", item.Stack()))
   end
   ImGui.EndGroup()

   ImGui.Dummy(TEXT_BASE_WIDTH, TEXT_BASE_HEIGHT)

   renderItemAttributes(item)
   ImGui.EndGroup()
end

local function renderItemComparison(item1, item2)
   ImGui.BeginGroup()

   local itemIdentifier = string.format("%s-%s-%s:%s-%s-%s", item1.ID(), item1.Name(), item1.ItemSlot(), item2.ID(), item2.Name(), item2.ItemSlot())

   ImGui.BeginGroup()

   renderTextWithBackground("Stat Differences", COLOR.DARKSLATEGRAY, itemIdentifier .. "-stat-changes-label-badge")

   ImGui.Dummy(TEXT_BASE_WIDTH, TEXT_BASE_HEIGHT / 3)

   renderWithBackground(
      function()
         local yPos = ImGui.GetCursorPosY()
         ImGui.SetCursorPosY(yPos - 1)
         renderItemIcon(item2, 18, 1, 1)
         ImGui.SameLine()
         ImGui.SetCursorPosY(yPos)
         ImGui.Text(item2.Name())
      end,
      COLOR.OLIVEDRAB,
      ImGui.CalcTextSize(item2.Name()) + TEXT_BASE_WIDTH + 25,
      TEXT_BASE_HEIGHT,
      getUIElementKey(itemIdentifier .. "-stat-changes-item2-badge")
   )

   ImGui.SameLine()
   renderTextWithBackground(ICON.MD_ARROW_FORWARD, COLOR.DARKSLATEGRAY, itemIdentifier .. "-stat-changes-arrow-badge")
   ImGui.SameLine()

   renderWithBackground(
      function()
         local yPos = ImGui.GetCursorPosY()
         ImGui.SetCursorPosY(yPos - 1)
         renderItemIcon(item1, 18, 1, 1)
         ImGui.SameLine()
         ImGui.SetCursorPosY(yPos)
        ImGui.Text(item1.Name())
      end,
      COLOR.DARKSLATEBLUE,
      ImGui.CalcTextSize(item1.Name()) + TEXT_BASE_WIDTH + 25,
      TEXT_BASE_HEIGHT,
      getUIElementKey(itemIdentifier .. "-stat-changes-item1-badge")
   )

   ImGui.NewLine()

   ImGui.EndGroup()

   for index, attributes in ipairs({BASE_ATTRIBUTES, ATTRIBUTES, ADVANCED_ATTRIBUTES, SAVES}) do
      local item1AttributeValues = getAttributes(item1, attributes)
      local item2AttributeValues = getAttributes(item2, attributes)

      for item1Index, item1Attribute in ipairs(item1AttributeValues) do
         for item2Index, item2Attribute in ipairs(item2AttributeValues) do
               if item2Attribute.attribute == item1Attribute.attribute then
               item1Attribute.value = item1Attribute.value - item2Attribute.value
               end
         end
      end

      renderAttributes(item, item1AttributeValues, true)
   end
   ImGui.EndGroup()
end

local function renderStackedItems(item)
   renderItemDetails(item)
   ImGui.SameLine()
   ImGui.Dummy(TEXT_BASE_WIDTH * 2, TEXT_BASE_HEIGHT)

   local wornSlots = getWornSlots(item)
   if #wornSlots > 0 then
      ImGui.SameLine()

      ImGui.BeginGroup()
      for wornItemIndex = 1, #wornSlots do
         local wornItem = mq.TLO.Me.Inventory(wornSlots[wornItemIndex])

         if wornItem and wornItem() then
            if wornItemIndex > 1 then
               ImGui.Dummy(TEXT_BASE_WIDTH, TEXT_BASE_HEIGHT * 1.5)
            end

            renderItemDetails(wornItem)

            ImGui.SameLine()
            ImGui.Dummy(TEXT_BASE_WIDTH * 1.5, TEXT_BASE_HEIGHT)
            ImGui.SameLine()
            renderItemComparison(item, wornItem)
         end
      end
      ImGui.EndGroup()
   end
end

local function renderItem(item)
   ImGui.BeginGroup()

   if not WORN_SLOT_NAMES[item.ItemSlot() + 1] and isWearable(item) then
      renderStackedItems(item)
   else
      renderItemDetails(item)
   end

   ImGui.EndGroup()
end

local showTooltip = false
local shouldDrawTooltip = false
local tooltipItem = nil
local tooltipOpen = false

local function renderTooltip(item)
   if ImGui.IsItemHovered() then
      ImGui.BeginTooltip()

      renderItem(item)

      ImGui.EndTooltip()
   end
end


return renderTooltip

-- enum ItemMembers
-- 	{
-- 		ID = 1,
-- 		Name = 2,
-- 		Lore = 3,
-- 		NoDrop = 4,
-- 		NoRent = 5,
-- 		Magic = 6,
-- 		Value = 7,
-- 		Size = 8,
-- 		Weight = 9,
-- 		Stack = 10,
-- 		Type = 11,
-- 		Charges = 12,
-- 		LDoNTheme = 13,
-- 		DMGBonusType = 14,
-- 		BuyPrice = 15,
-- 		Haste = 16,
-- 		Endurance = 17,
-- 		Attack = 18,
-- 		HPRegen = 19,
-- 		ManaRegen = 20,
-- 		DamShield = 21,
-- 		WeightReduction = 22,
-- 		SizeCapacity = 23,
-- 		Combinable = 24,
-- 		Skill = 25,
-- 		Avoidance = 26,
-- 		SpellShield = 27,
-- 		StrikeThrough = 28,
-- 		StunResist = 29,
-- 		Shielding = 30,
-- 		FocusID = 31,
-- 		ProcRate = 32,
-- 		Quality = 33,
-- 		LDoNCost = 34,
-- 		AugRestrictions = 35,
-- 		AugType = 36,
-- 		AugSlot1 = 37,
-- 		AugSlot2 = 38,
-- 		AugSlot3 = 39,
-- 		AugSlot4 = 40,
-- 		AugSlot5 = 41,
-- 		AugSlot6 = 42,
-- 		Damage = 43,
-- 		Range = 44,
-- 		DMGBonus = 45,
-- 		RecommendedLevel = 46,
-- 		RecommendedSkill = 47,
-- 		Delay = 48,
-- 		Light = 49,
-- 		Level = 50,
-- 		BaneDMG = 51,
-- 		SkillModValue = 52,
-- 		InstrumentType = 53,
-- 		InstrumentMod = 54,
-- 		RequiredLevel = 55,
-- 		BaneDMGType = 56,
-- 		AC = 57,
-- 		HP = 58,
-- 		Mana = 59,
-- 		STR = 60,
-- 		STA = 61,
-- 		AGI = 62,
-- 		DEX = 63,
-- 		CHA = 64,
-- 		INT = 65,
-- 		WIS = 66,
-- 		svCold = 67,
-- 		svFire = 68,
-- 		svMagic = 69,
-- 		svDisease = 70,
-- 		svPoison = 71,
-- 		Summoned = 72,
-- 		Artifact = 73,
-- 		PendingLore = 74,
-- 		LoreText = 75,
-- 		Items = 76,
-- 		Item = 77,
-- 		Container = 78,
-- 		Stackable = 79,
-- 		InvSlot = 80,
-- 		SellPrice = 81,
-- 		WornSlot = 82,
-- 		WornSlots = 83,
-- 		CastTime = 84,
-- 		Spell = 85,
-- 		EffectType = 86,
-- 		Tribute = 87,
-- 		Attuneable = 88,
-- 		Timer = 89,
-- 		ItemDelay = 90,
-- 		TimerReady = 91,
-- 		StackSize = 92,
-- 		Stacks = 93,
-- 		StackCount = 94,
-- 		FreeStack = 95,
-- 		MerchQuantity = 96,
-- 		Classes = 97,
-- 		Class = 98,
-- 		Races = 99,
-- 		Race = 100,
-- 		Deities = 101,
-- 		Deity = 102,
-- 		Evolving = 103,
-- 		svCorruption = 104,
-- 		Power = 105,
-- 		MaxPower = 106,
-- 		Purity = 107,
-- 		Accuracy = 108,
-- 		CombatEffects = 109,
-- 		DoTShielding = 110,
-- 		HeroicSTR = 111,
-- 		HeroicINT = 112,
-- 		HeroicWIS = 113,
-- 		HeroicAGI = 114,
-- 		HeroicDEX = 115,
-- 		HeroicSTA = 116,
-- 		HeroicCHA = 117,
-- 		HeroicSvMagic = 118,
-- 		HeroicSvFire = 119,
-- 		HeroicSvCold = 120,
-- 		HeroicSvDisease = 121,
-- 		HeroicSvPoison = 122,
-- 		HeroicSvCorruption = 123,
-- 		EnduranceRegen = 124,
-- 		HealAmount = 125,
-- 		Clairvoyance = 126,
-- 		DamageShieldMitigation = 127,
-- 		SpellDamage = 128,
-- 		Augs = 129,
-- 		Tradeskills = 130,
-- 		ItemSlot = 131,
-- 		ItemSlot2 = 132,
-- 		Address = 133,
-- 		PctPower = 134,
-- 		Prestige = 135,
-- 		FirstFreeSlot = 136,
-- 		SlotsUsedByItem = 137,
-- 		Heirloom = 138,
-- 		Collectible = 139,
-- 		NoDestroy = 140,
-- 		Quest = 141,
-- 		Expendable = 142,
-- 		ContAddress = 143,
-- 		ItemLink = 144,
-- 		Icon = 145,
-- 		SkillModMax = 146,
-- 		OrnamentationIcon = 147,
-- 		ContentSize = 148,
-- 		Open = 149,
-- 		NoTrade = 150,
-- 		AugSlot = 151,
-- 		Clicky = 152,
-- 		Proc = 153,
-- 		Worn = 154,
-- 		Focus = 155,
-- 		Scroll = 156,
-- 		Focus2 = 157,
-- 		Mount = 158,
-- 		Illusion = 159,
-- 		Familiar = 160,
-- 		CanUse = 161,
-- 		LoreEquipped = 162,
-- 		Luck = 163,
-- 		MinLuck = 164,
-- 		MaxLuck = 165,
-- 	};
